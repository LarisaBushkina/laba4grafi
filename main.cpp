#include <iostream>
#include <ctime>
#include <header.h>


int main()
{
    Graph<int>A(6); //Создание графа с 5 вершинами



    A.setNode(1, 65);
    A.setNode(2, 52);
    A.setNode(3, 73);
    A.setNode(4, 54);
    A.setNode(5, 43);
    A.setNode(6, 45);

    A.showNode(); //Вывод на экран значений вершин
    cout << endl;


    cout << endl;

    A.showAdjacencyMatrix(); //Вывод на экран матрицы смежности

    cout << endl;

    A.addNode(5); //Добавление вершины

    A.addConnectionAllListAdjacency(); //Инициализация матрицы смежности через списки смежности
    cout << endl;

    A.delNode(7);

    A.showNode(); //Вывод на экран значений вершин
    cout << endl;

    A.showAdjacencyMatrix(); //Вывод на экран матрицы смежности

    cout << endl;

    cout << A.existencePath(4, 2) << endl; // Проверка существует ли путь

    cout << endl;

    cout << A.getNode(1) << endl;
    cout << A.getNode(2) << endl;
    cout << A.getNode(3) << endl;
    cout << A.getNode(4) << endl;
    cout << A.getNode(5) << endl;
    cout << A.getNode(6) << endl;

    A.clear();

    return 0;
}
