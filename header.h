#ifndef HEADER_H
#define HEADER_H

#include<iostream>
#include<vector>
using namespace std;

template <class T>
class Graph
{
    int SizeNode;
    T* Node;
    int** AdjacencyMatrix;

public:

    void setNode(int index, T value);

    T getNode(int index);

    Graph(int sizeV); //конструктор

    T &operator[] (int index); //Перегрузка []

    bool existencePath(int Node1, int Node2); // Существует ли путь между двумя вершинами. true - да, false - нет

    int sizeNode(); //Получение колличества вершин

    void showNode(); //Вывод на экран значений вершин

    void showAdjacencyMatrix(); //Вывод на экран матрицы смежности

    void addNode(T value); //Добавить вершину

    void delNode(int index); //Удаление вершины и связей с этой вершиной

    void addConnectionListAdjacency(int index); //Добавление связей через список одной вершины

    void addConnectionAllListAdjacency(); //Добавление связей через список всех вершин

    void delConnectionListAdjacency(int index); //Удаление связей через список одной вершины

    void clearConnection(); //Удаление связей (иначе говоря очистка матрицы смежности)

    void clearNode(); //Обнуление значений вершин

    void clear(); //Удаление связей и обнуление значений вершин



};

template <class T>
Graph<T>::Graph(int sizeV)  //конструктор
{
    SizeNode = sizeV;
    Node = new T[SizeNode];

    AdjacencyMatrix = new int*[SizeNode];
    for (int i = 0; i<SizeNode; i++)
    {
        AdjacencyMatrix[i] = new int[SizeNode];
    }

    for(int i = 0; i<SizeNode; i++)
        for(int j = 0; j<SizeNode; j++)
        {
            AdjacencyMatrix[i][j] = 0;
        }
}

template <class T>
T &Graph<T>::operator [](int index) //Перегрузка []
{
    if(index>=0 && index<SizeNode)
    {
        return Node[index];
    }
    else
        throw "Out of graph's element.";
}


template <class T> // Существует ли путь между двумя вершинами. true - да, false - нет
bool Graph<T>::existencePath(int Node1, int Node2)
{
    Node1--;
    Node2--;
    if(Node1 < SizeNode && Node1>=0 && Node2 < SizeNode && Node2>=0)
    {
        // Копирование матрицы смежности, которую можно было бы в последствии изменять
        int** Buff = new int*[SizeNode];
        for(int i = 0; i<SizeNode; i++)
        {
            Buff[i] = new int[SizeNode];
        }

        for (int i = 0; i<SizeNode; i++)
            for (int j = 0; j<SizeNode; j++)
            {
                Buff[i][j] = AdjacencyMatrix[i][j];
            }

        // Поиск пути
        bool end = false;
        int i = Node1;
        int j = 0;
        int CounterMotion = 1;

        vector <int> save(1);
        save[0] = Node1;

        while (end == false)
        {
            if (Buff[i][j] == 1)
            {
                Buff[i][j] = 0;
                save.push_back(i);
                i = j;
                j = 0;
                CounterMotion++;
                if (i == Node2)
                {
                    return true;
                    end = true;
                }
            }

            else if (Buff[i][j] == 0)
            {
                j++;
                if (j == SizeNode)
                {
                    CounterMotion--;
                    if (CounterMotion != 0)
                    {
                        i = save[CounterMotion];
                        j = 0;
                    }

                    else if (CounterMotion == 0)
                    {
                        return false;
                        end = true;
                    }
                }
            }
        }
        delete [] Buff;
    }

    else
        throw "Nonexistent vertices";

    return 0;
}


template <class T> //Получение колличества вершин
int Graph<T>::sizeNode()
{
    return SizeNode;
}



template <class T> //Вывод на экран значений вершин
void Graph<T>::showNode()
{
    for(int i=0; i<SizeNode; i++)
    {
        cout << "Node " << i+1 << ": " << Node[i] << endl;
    }
}

template <class T> //Вывод на экран матрицы смежности

void Graph<T>::showAdjacencyMatrix()
{
    for(int i = 0; i<SizeNode; i++)
    {
        for(int j = 0; j<SizeNode; j++)
        {
            cout << AdjacencyMatrix[i][j] << " ";
        }
        cout << endl;
    }
}

template <class T> //Добавить вершину
void Graph<T>::addNode(T value)
{
    //Добавление вершины
    T* buffer = new T[SizeNode];
    for(int i=0; i<SizeNode; i++)
    {
        buffer[i]=Node[i];
    }
    delete[] Node;
    Node = new T[SizeNode + 1];
    for(int i=0; i<SizeNode; i++)
    {
        Node[i]=buffer[i];
    }
    Node[SizeNode] = value;
    SizeNode++;

    //Добавление строки и столбца в матрицу смежности
    int** bufferMatrix = new int*[SizeNode];
    for (int i = 0; i<SizeNode; i++)
    {
        bufferMatrix[i] = new int[SizeNode];
    }

    for(int i = 0; i<SizeNode - 1; i++)
        for(int j = 0; j<SizeNode - 1; j++)
        {
            bufferMatrix[i][j] = AdjacencyMatrix[i][j];
        }

    delete [] AdjacencyMatrix;

    AdjacencyMatrix = new int*[SizeNode];
    for (int i = 0; i<SizeNode; i++)
    {
        AdjacencyMatrix[i] = new int[SizeNode];
    }

    for(int i = 0; i<SizeNode - 1; i++)
        for(int j = 0; j<SizeNode - 1; j++)
        {
            AdjacencyMatrix[i][j] = bufferMatrix[i][j];
        }

    for (int i = 0; i<SizeNode; i++)
    {
        AdjacencyMatrix[SizeNode-1][i] = 0;
        AdjacencyMatrix[i][SizeNode-1] = 0;
    }
}

template <class T> //Добавление связей через список одной вершины
void Graph<T>::addConnectionListAdjacency(int index)
{
    if (index > 0 && index <= SizeNode)
    {
        char ch = '0';

        cout << "Connection with " << index << " Node: ";
        while (ch != '\n')
        {
            int index2;
            cin >> index2;
            if ((index2>0) && (index2<=SizeNode))
            {
                AdjacencyMatrix[index-1][index2-1] = 1;
            }
            ch = cin.get();
            if (ch == '\n')
                break;
        }
    }
}

template <class T> //Добавление связей через список всех вершин
void Graph<T>::addConnectionAllListAdjacency()
{
    for(int i = 1; i<=SizeNode; i++)
    {
        addConnectionListAdjacency(i);
    }
}

template <class T>//Удаление связей через список одной вершины
void Graph<T>::delConnectionListAdjacency(int index)
{
    if (index > 0 && index <= SizeNode)
    {
        char ch = '0';

        cout << "Delete connection with " << index << " Node: ";
        while (ch != '\n')
        {
            int index2;
            cin >> index2;
            if ((index2>0) && (index2<=SizeNode))
            {
                AdjacencyMatrix[index-1][index2-1] = 0;
            }
            ch = cin.get();
            if (ch == '\n')
                break;
        }
    }

}

template <class T> //Удаление связей ( очистка матрицы смежности)
void Graph<T>::clearConnection()
{
    for(int i = 0; i<SizeNode; i++)
        for (int j = 0; j<SizeNode; j++)
        {
            AdjacencyMatrix[i][j] = 0;
        }
}

template <class T> //Обнуление значений вершин
void Graph<T>::clearNode()
{
    for(int i = 0; i<SizeNode; i++)
        Node[i] = 0;
}

template <class T> //Удаление связей и обнуление значений вершин
void Graph<T>::clear()
{
    clearConnection();
    clearNode();
}

template<class T> //Удаление вершины и связей с этой вершиной
void Graph<T>::delNode(int index)
{
    //Удаление вершины
    index--;
    T* bufferNode = new T[SizeNode-1];
    for(int i = 0; i<index; i++)
    {
        bufferNode[i] = Node[i];
    }

    for(int i = index+1; i<SizeNode; i++)
    {
        bufferNode[i-1] = Node[i];
    }
    delete[] Node;
    Node = new T[SizeNode-1];

    SizeNode--;

    for(int i = 0; i<SizeNode; i++)
    {
        Node[i] = bufferNode[i];
    }

    delete[] bufferNode;

    //Удаление строки и столбца из матрицы смежности
    int** bufferMatrix = new int*[SizeNode];
    for(int i = 0; i<SizeNode; i++)
    {
        bufferMatrix[i] = new int[SizeNode];
    }

    for(int i = 0; i<index; i++)
        for(int j = 0; j<index; j++)
        {
            bufferMatrix[i][j] = AdjacencyMatrix[i][j];
        }

    for(int i = index; i<SizeNode; i++)
        for(int j = 0; j<index; j++)
        {
            bufferMatrix[i][j] = AdjacencyMatrix[i+1][j];
        }

    for(int i = 0; i<index; i++)
        for(int j = index; j<SizeNode; j++)
        {
            bufferMatrix[i][j] = AdjacencyMatrix[i][j+1];
        }

    for(int i = index; i<SizeNode; i++)
        for(int j = index; j<SizeNode; j++)
        {
            bufferMatrix[i][j] = AdjacencyMatrix[i+1][j+1];
        }

    delete[] AdjacencyMatrix;

    AdjacencyMatrix = new int*[SizeNode];
    for (int i = 0; i<SizeNode; i++)
    {
        AdjacencyMatrix[i] = new int[SizeNode];
    }

    for(int i = 0; i<SizeNode; i++)
        for(int j = 0; j<SizeNode; j++)
        {
            AdjacencyMatrix[i][j] = bufferMatrix[i][j];
        }
}


template <class T>
void Graph<T>::setNode(int index, T value)
{
    Node[index-1] = value;
}

template <class T>
T Graph<T>::getNode(int index)
{
    return Node[index-1];
}

#endif // HEADER_H

